# ly
> Ly is a lightweight TUI (ncurses-like) display manager for Linux and BSD.<sup>[1][desc]</sup>

**Links:** [Source Code][code]

## Notes:
- Stow: sudo stow to /
- Enable systemd service:\
`sudo systemctl enable ly.service`


[desc]: https://github.com/fairyglade/ly
[code]: https://github.com/fairyglade/ly
