# Waybar
[Waybar][waybar] is a highly customizable Wayland bar for Sway and Wlroots based compositors.<sup>[1][waybar-desc]</sup>

[waybar]: https://github.com/Alexays/Waybar
[waybar-desc]: https://github.com/Alexays/Waybar/blob/master/README.md
