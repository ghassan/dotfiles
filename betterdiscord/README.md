# BetterDiscord
> A client modification for [Discord][site1]. This allows you to add plugins and themes to your personal copy of Discord. BetterDiscord also adds a number of other features out of the box.<sup>[1][desc]</sup>

**Links:** [Website][site] - [Source Code][code] - [Documentation][docs] - [Package][pkg]

## Directions:
- Install [discord][site1]
- Install [betterdiscordctl][site2] from the [aur][pkg]
- Run `betterdiscordctl install`


[site]: https://betterdiscord.app
[desc]: https://github.com/BetterDiscord/BetterDiscord
[code]: https://github.com/BetterDiscord/BetterDiscord
[docs]: https://betterdiscord.app/Docs
[site1]: https://discord.com/
[site2]: https://github.com/bb010g/betterdiscordctl
[pkg]: https://aur.archlinux.org/packages/betterdiscordctl
