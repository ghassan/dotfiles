# ⚙️ dotfiles 🔧
My laptop configs

## Overview
- **💻 Distro:** Arch Linux
- **🎨 Theme:** Nord
- **🔳 Display Server:** Wayland *(+ xwayland)*

## CLI Utilities
- **🔆 [brightnessctl](brightnessctl/README.md) ![osi] -** Control device brightness
- **🔊 [pamixer](pamixer/README.md) ![osi] -** Control device volume
- **✂️ [wl-clipboard](wl-clipboard/README.md) ![osi] -** Clipboard
<details>
  <summary>Also Installed</summary>

- **📁 exa ![][osi] -** Modern `ls`
- **🔍 fd ![osi] -** Alternative to `find`
- **🗜 atool ![osi] -** Manage archives (zit, tar, etc.)
- **📖 tldr ![osi] -** Simplified man pages
- **🔨 ansible ![osi] -** 
- **📄 bat ![osi] -** Alternative to `cat`
- **🔗 stow ![osi] -** Symlink manager for dotfiles
- **#️⃣ tea ![osi] -** CLI tool to manage gitea
</details>
<details>
  <summary>Consider Installing</summary>

- **🗄️ [duf](https://github.com/muesli/duf) -** `df` alternative
- **🔎 [fzf](https://github.com/junegunn/fzf) -** CLI fuzzy finder
- **🔔 [undistract-me](https://github.com/jml/undistract-me) -** Notifies when slow terminal commands finish
- **🤬 [The Fuck](https://github.com/nvbn/thefuck) -** Corrects last console command
- **[Color LS](https://github.com/athityakumar/colorls) -** `ls` with colors and icons
- **[lsd](https://github.com/Peltoche/lsd) -** next gen `ls`
</details>

## Utilities
- **📟 ![osi] [kitty](kitty/README.md) -** Terminal
- **🐚 ![osi] [zsh](zsh/README.md) -** Shell
- **🔔 ![osi] [mako](mako/README.md) -** Notification daemon
- **⌨️ ![osi] [wvkbd](wvkbd/README.md) -** On-screen keyboard

## Display
- **🖥 [ly](ly/README.md) -** Display manage ![osi]
- **🪟 [Sway](sway/README.md) -** Wayland compositor and tiling window manager ![osi]
- **🔒 [swaylock](swaylock/README.md) -** Lock screen ![osi]
- **➖ [waybar](waybar/README.md) -** Status bar ![osi]
<details>
  <summary>Consider Installing</summary>

- **swayidle -** 
</details>
<details>
  <summary>Old</summary>

- **🪟 [river](river/README.md) -** Wayland compositor and tiling window manager
- **qtile -** Windows Manager configured in python\
- **⚙ Xresources -** Config files for Xresources
</details>

## Applications
- **🌐 [Firefox](firefox/README.md) -** Internet browser ![osi]
- **🇻 [tridactyl](tridactyl/README.md) -** Vi-bindings for Firefox ![osi]
- **🌐 [qutebrowser](qutebrowser/README.md) -** Internet browser with vim bindings ![osi]
<details>
  <summary>Also Installed</summary>

- **🔐 Bitwarden -** Password manager ![osi]
- **🔄 Syncthing -** File sync ![osi]
</details>

## TUI Applications
- **📝 [Neovim](neovim/README.md) -** Text editor based on vim ![osi]
- **🗃 [ranger](ranger/README.md) -** TUI file explorer with vi bindings ![osi]
- **🔳 [Tmux](tmux/README.md) -** Terminal multiplexer ![osi]
- **🚀 [wofi](wofi/README.md) -** application launcher ![osi]

## Media Players
- **🎬🎞️ [mpv](mpv/README.md) -** video player ![osi]
- **🖼️ [vimiv](vimiv/README.md) -** image viewer ![osi]
- **📃 [zathura](zathura/README.md) -** PDF/ePub Viewer ![osi]
- **🎨 [Spicetify](spicetify/README.md) -** Spotify theming ![osi]
<details>
  <summary>Also Installed</summary>

- **Spotify 🎵🎶 -** Spotify client
</details>
<details>
  <summary>Consider Installing</summary>

- **🖼️ [imv](imv/README.md) -** image viewer
- **🖼️ [mvi](mvi/README.md) -** image viewer
- **🔤 [OSD Lyrics](https://github.com/osdlyrics/osdlyrics) -** Show synced lyrics with your favorite media player on Linux
</details>

## Productivity
<details>
  <summary>Consider Installing</summary>

- **🗓️ [calcurse](calcurse/README.md) -** TUI calendar
- **📝📋 [Taskwarrior](taskwarrior/README.md) -** TUI TODO list
- **📄 [LibreOffice](https://www.libreoffice.org/) -** Office suite
- **📄 [Calligra](https://www.libreoffice.org/) -** Office suite
</details>

## Communication
- **🎨 [BetterDiscord](betterdiscord/README.md) -** Discord theming ![osi]
<details>
  <summary>Also Installed</summary>

- **💬 Discord -** Discord client
- **💬 Element -** Matrix client ![osi]
- **💬 Telegram -** Telegram client ![osi]
- **💬 Signal -** Signal client ![osi]
</details>

## Extra
- **🎨 [GTK](gtk/README.md) -** Config files for GTK 2.0 & GTK 3.0 ![osi]
- **🎨 [dir_colors](dir_colors/README.md) -** Theme for `ls` and `tree` ![osi]
- **⚛️ [electron](electron/README.md) -** Config files for electron apps ![osi]

## Games
<details>
  <summary>Consider Installing</summary>

- **🎮 [0 A.D.](https://play0ad.com/) -** A free, open-source, historical Real Time Strategy (RTS) game 
- **🎮 [FreeCiv](https://www.freeciv.org/) -** A Free and Open Source empire-building strategy game inspired by the history of human civilization
- **🎮 [Heroic](https://heroicgameslauncher.com/) -** Open Source GOG and Epic games launcher
</details>

## Android
<details>
  <summary>Also Installed</summary>

- **📱 KDE Connect -** 
</details>
<details>
  <summary>Consider Installing</summary>

- **📱 [Anbox](https://anbox.io/) -** Run Android applications on any GNU/Linux operating system.
- **📱 [Waydroid](https://waydro.id/) -** Waydroid uses a container-based approach to boot a full Android system on a regular GNU/Linux system like Ubuntu.
- **📱 [srccpy](https://github.com/Genymobile/scrcpy) -** Display and control your Android device
- **📱 [guiscrcpy](https://github.com/srevinsaju/guiscrcpy) -** Open Source GUI based Android Screen Mirroring System
</details>

## Fonts
- **😀 Twemoji -** Twitter emoji font ![osi]
- **🔤 FiraGO -** Font ![osi]
- **🔤 Fura Code -** Monospaced Fira Code font with Nerd Font patch ![osi]
- **🔤 Amiri Typewriter -** Arabic Font (with monospaced version) ![osi]


## stowd
Script to easily symlink configs using stow.
- Create folder for an app and include all its configs as they appear from your home directory (or root).
  - Will usually look like: `app_name/.config/app_name/config`
  - for root: `etc/app_name/config`
- Edit `stowd.yaml` to include configs to add or remove
- Run `python stowd.py`
  - Will get prompted for password for root configs

[osi]: ./.img/osi.svg
