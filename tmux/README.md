# tmux
[tmux][tmux] is a terminal multiplexer: it enables a number of terminals to be created, accessed, and controlled from a single screen. tmux may be detached from a screen and continue running in the background, then later reattached.<sup>[1][tmux-desc]</sup>

[tmux]: https://github.com/tmux/tmux
[tmux-desc]: https://github.com/tmux/tmux/blob/master/README
