# mako
[mako][mako] is a lightweight notification daemon for Wayland compositors that support the layer-shell protocol.<sup>[1][mako-desc]</sup>

[mako]: https://wayland.emersion.fr/mako
[mako-desc]: https://wayland.emersion.fr/mako
[mako-code]: https://github.com/emersion/mako
[mako-wiki]: https://github.com/emersion/mako/wiki
