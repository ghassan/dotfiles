# ZSH
[Zsh][zsh] is a powerful shell that operates as both an interactive shell and as a scripting language interpreter.<sup>[1][zsh-desc]</sup>

[zsh]: https://wiki.archlinux.org/title/Zsh
[zsh-desc]: https://wiki.archlinux.org/title/Zsh
