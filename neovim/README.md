# 📝 Neovim:
[Neovim][neovim] is a hyperextensible Vim-based text editor.<sup>[1][neovim-desc]</sup>


- init.vim *(config file)*
    - Unix: *\~/.config/nvim/init.vim*
    - Windows: *\~/AppData/Local/nvim/init.vim*
- [vim-plug][vim-plug] - *(Plugin manager)*
- [nord-vim][nord-vim] - *(color scheme)*
- [Markdown-Preview.nvim][markdown-preview] - *(Markdown preview in browser)*
- ~~[coc-nvim][coc-nvim]~~ - 
- ~~[indentLine][indentline]~~ - *(show indent guides)*
- ~~[NERDTree][nerdtree]~~ - *(Directory manager)*

[neovim]: https://neovim.io/
[neovim-desc]: https://neovim.io/
[neovim-code]: https://github.com/neovim/neovim
[neovim-wiki]: https://wiki.archlinux.org/title/Neovim
[vim-plug]: https://github.com/junegunn/vim-plug
[nord-vim]: https://github.com/arcticicestudio/nord-vim
[markdown-preview]: https://github.com/iamcco/markdown-preview.nvim
[nerdtree]: https://github.com/preservim/nerdtree
[indentline]: https://github.com/Yggdroot/indentLine
[coc-nvim]: https://github.com/neoclide/coc.nvim
