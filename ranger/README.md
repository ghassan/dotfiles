# [ranger][ranger]
> A console file manager with VI key bindings. It provides a minimalistic and nice curses interface with a view on the directory hierarchy. It ships with rifle, a file launcher that is good at automatically finding out which program to use for what file type.<sup>[1][ranger-desc]</sup>

[ranger]: https://github.com/ranger/ranger
[ranger-desc]: https://github.com/ranger/ranger/blob/master/README.md
