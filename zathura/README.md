# [zathura][zathura]
> A highly customizable document viewer with vi-styled keybindings. It provides a minimalistic and space-saving interface. Users interact with zathura primarily with the keyboard. Different file formats are supported through plugins. Support is available for PDF, PS, DjVu and comic book files.<sup>[1][zathura-desc]</sup>

**Links:** [Website][zathura] - [Source Code][zathura-code] - [Arch Wiki][zathura-aw] - [Wikipedia][zathura-wiki]

[zathura]: https://pwmt.org/projects/zathura/
[zathura-desc]: https://wiki.archlinux.org/title/Zathura
