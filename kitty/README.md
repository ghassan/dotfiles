# kitty
[kitty][kitty] is a fast, feature-rich, cross-platform, GPU based terminal.<sup>[1][kitty-desc]</sup>

[kitty]: https://sw.kovidgoyal.net/kitty/
[kitty-desc]: https://sw.kovidgoyal.net/kitty/
[kitty-code]: https://github.com/kovidgoyal/kitty
[kitty-wiki]: https://wiki.archlinux.org/title/Kitty
